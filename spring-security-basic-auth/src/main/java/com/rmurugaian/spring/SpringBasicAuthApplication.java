package com.rmurugaian.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author rmurugaian 2018-05-18
 */
@SpringBootApplication
public class SpringBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBasicAuthApplication.class, args);
    }
}
